import { useState } from 'react';
import './App.css';
import Axios from 'axios'

function App() {

  const [nama, setNama] = useState("");
  const [umur, setUmur] = useState("0");
  const [kotaasal, setKotaAsal] = useState("");
   
  const addUser = () => {
    Axios.post('http://localhost:3001/create',{
      nama : nama,
      umur : umur, 
      kotaasal : kotaasal,
    }).then(() => {
      console.log("berhasil"); 
    });
  };

  return (
    <div className="App">
      <div className="data">
        <lebel>Nama :</lebel>
        <input 
          type="text" 
          onChange={(event) => {
            setNama(event.target.value);
            }}
        />
        <lebel>Umur :</lebel>
        <input 
          type="number"
          onChange={(event) => {
            setUmur(event.target.value);
            }} 
        />
        <lebel>Kota Asal :</lebel>
        <input 
          type="text"
          onChange={(event) => {
            setKotaAsal(event.target.value);
            }} 
        />
        <button onClick={addUser}>Tambah data</button>
      </div>
    </div>
  );
}

export default App;